from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
# from users.models import Profile

# Custom users and the built-in auth forms
# https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#custom-users-and-the-built-in-auth-forms

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


# Model Form
# https://docs.djangoproject.com/en/4.0/topics/forms/modelforms/#modelform

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email']


# class ProfileUpdateForm(forms.ModelForm):
#     class Meta:
#         model = Profile
#         fields = ['image']