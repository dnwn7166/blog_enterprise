# from PIL import Image
# from django.db import models
# from django.contrib.auth.models import User
#
#
# # Create your models here.
#
#
# class Profile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     image = models.ImageField(default='default.jpg', upload_to='profile_pics')
#
#     # OneToOne Relationship
#     # https://docs.djangoproject.com/en/4.0/topics/db/examples/one_to_one/
#
#     # ImageField
#     # https://docs.djangoproject.com/en/4.0/ref/forms/fields/#imagefield
#
#     def __str__(self):
#         return f'{self.user.username} Profile'
#
#     def save(self):
#         super().save()
#         img = Image.open(self.image.path)
#
#         if img.height > 300 or img.width > 300:
#             output_size = (300, 300)
#             img.thumbnail(output_size)
#             img.save(self.image.path)
